# SwiftNet-Segmenter - Convolutional-Transformer Hybrid Architecture

Model created as a hybrid model, combination of:

-Segmenter (transformer for semantic segmentation, **see also** my repo researching Segmenter models: https://gitlab.com/artificial-intelligence-machine-learning-deep-learning/computer-vision/graduate-diploma-thesis-project/segmenter-transformer-models-for-semantic-segmentation, official repo: https://github.com/rstrudel/segmenter) and

-SwiftNet (convolutional model, **see also** my repo researching SwiftNet models: https://gitlab.com/artificial-intelligence-machine-learning-deep-learning/computer-vision/graduate-diploma-thesis-project/swiftnet-convolutional-models-with-pyramidal-fusion-for-semantic-segmentation, official repo: https://github.com/orsic/swiftnet) models. 

Implemented in Python using PyTorch library.

Creating and implementing novel hybrid architecture variants in form of combinations of the two dominant models in computer vision, which are convolutional and transformer models, is an interesting area of research and experimentation.

Researching, designing, training and evaluating these models for semantic segmentation is one of the main parts of my grad thesis deep learning project, which has the goal of comparing state-of-the-art transformer to convolutional models.

Training and evaluating SwiftNet-Segmenter models in different architectures variants and conducting corresponding hyperparameter validation.

Initial version of architecture, where Segmenter was trained on small images (subsampled interpolated images or center crops given as input to Segmenter at coarsest level of image pyramid), was changed to the current implementation, which is designed better - Segmenter is now trained on entire images and then validated on entire or small images.

Project duration: Feb 2022 - July 2022.
