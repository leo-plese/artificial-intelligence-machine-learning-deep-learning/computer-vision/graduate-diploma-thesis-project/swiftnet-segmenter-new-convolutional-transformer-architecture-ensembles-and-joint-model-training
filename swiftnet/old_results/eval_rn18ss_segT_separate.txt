SWIFT

Errors:
        road IoU accuracy = 94.96 %
        sidewalk IoU accuracy = 78.64 %
        building IoU accuracy = 91.34 %
        wall IoU accuracy = 46.35 %
        fence IoU accuracy = 50.80 %
        pole IoU accuracy = 60.43 %
        traffic light IoU accuracy = 68.08 %
        traffic sign IoU accuracy = 75.82 %
        vegetation IoU accuracy = 92.02 %
        terrain IoU accuracy = 57.86 %
        sky IoU accuracy = 94.52 %
        person IoU accuracy = 80.76 %
        rider IoU accuracy = 59.53 %
        car IoU accuracy = 94.31 %
        truck IoU accuracy = 70.47 %
        bus IoU accuracy = 84.04 %
        train IoU accuracy = 70.85 %
        motorcycle IoU accuracy = 54.29 %
        bicycle IoU accuracy = 75.42 %
IoU mean class accuracy -> TP / (TP+FN+FP) = 73.71 %
mean class recall -> TP / (TP+FN) = 80.63 %
mean class precision -> TP / (TP+FP) = 88.32 %
pixel accuracy = 94.88 %



SEG
>>> original
Errors:
        road IoU accuracy = 97.74 %
        sidewalk IoU accuracy = 81.61 %
        building IoU accuracy = 90.14 %
        wall IoU accuracy = 57.22 %
        fence IoU accuracy = 51.60 %
        pole IoU accuracy = 44.35 %
        traffic light IoU accuracy = 55.52 %
        traffic sign IoU accuracy = 67.29 %
        vegetation IoU accuracy = 90.65 %
        terrain IoU accuracy = 62.65 %
        sky IoU accuracy = 93.48 %
        person IoU accuracy = 73.73 %
        rider IoU accuracy = 49.37 %
        car IoU accuracy = 92.63 %
        truck IoU accuracy = 64.48 %
        bus IoU accuracy = 77.80 %
        train IoU accuracy = 64.01 %
        motorcycle IoU accuracy = 54.03 %
        bicycle IoU accuracy = 68.66 %
IoU mean class accuracy -> TP / (TP+FN+FP) = 70.37 %
mean class recall -> TP / (TP+FN) = 79.30 %
mean class precision -> TP / (TP+FP) = 84.30 %
pixel accuracy = 94.82 %
val: swift = 73.71  .  seg = 70.37

>>> decreased resolution
Errors:
        road IoU accuracy = 97.26 %
        sidewalk IoU accuracy = 78.49 %
        building IoU accuracy = 88.96 %
        wall IoU accuracy = 51.23 %
        fence IoU accuracy = 46.67 %
        pole IoU accuracy = 38.05 %
        traffic light IoU accuracy = 47.63 %
        traffic sign IoU accuracy = 61.05 %
        vegetation IoU accuracy = 88.90 %
        terrain IoU accuracy = 58.30 %
        sky IoU accuracy = 92.49 %
        person IoU accuracy = 68.32 %
        rider IoU accuracy = 44.53 %
        car IoU accuracy = 90.77 %
        truck IoU accuracy = 67.91 %
        bus IoU accuracy = 74.23 %
        train IoU accuracy = 55.46 %
        motorcycle IoU accuracy = 48.30 %
        bicycle IoU accuracy = 63.64 %
IoU mean class accuracy -> TP / (TP+FN+FP) = 66.43 %
mean class recall -> TP / (TP+FN) = 76.03 %
mean class precision -> TP / (TP+FP) = 81.55 %
pixel accuracy = 93.90 %
val: swift = 73.71  .  seg = 66.43


>>> center crop
Errors:
        road IoU accuracy = 60.20 %
        sidewalk IoU accuracy = 8.93 %
        building IoU accuracy = 21.48 %
        wall IoU accuracy = 4.55 %
        fence IoU accuracy = 2.27 %
        pole IoU accuracy = 1.20 %
        traffic light IoU accuracy = 0.24 %
        traffic sign IoU accuracy = 1.10 %
        vegetation IoU accuracy = 15.57 %
        terrain IoU accuracy = 2.56 %
        sky IoU accuracy = 0.44 %
        person IoU accuracy = 5.06 %
        rider IoU accuracy = 0.64 %
        car IoU accuracy = 8.47 %
        truck IoU accuracy = 4.85 %
        bus IoU accuracy = 3.59 %
        train IoU accuracy = 1.43 %
        motorcycle IoU accuracy = 1.33 %
        bicycle IoU accuracy = 4.00 %
IoU mean class accuracy -> TP / (TP+FN+FP) = 7.79 %
mean class recall -> TP / (TP+FN) = 13.87 %
mean class precision -> TP / (TP+FP) = 15.82 %
pixel accuracy = 45.08 %
val: swift = 73.71  .  seg = 7.79
