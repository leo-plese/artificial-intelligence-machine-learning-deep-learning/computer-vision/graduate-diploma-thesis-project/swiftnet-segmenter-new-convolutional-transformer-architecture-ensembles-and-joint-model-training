SWIFT

Errors:
        road IoU accuracy = 94.96 %
        sidewalk IoU accuracy = 78.64 %
        building IoU accuracy = 91.34 %
        wall IoU accuracy = 46.35 %
        fence IoU accuracy = 50.80 %
        pole IoU accuracy = 60.43 %
        traffic light IoU accuracy = 68.08 %
        traffic sign IoU accuracy = 75.82 %
        vegetation IoU accuracy = 92.02 %
        terrain IoU accuracy = 57.86 %
        sky IoU accuracy = 94.52 %
        person IoU accuracy = 80.76 %
        rider IoU accuracy = 59.53 %
        car IoU accuracy = 94.31 %
        truck IoU accuracy = 70.47 %
        bus IoU accuracy = 84.04 %
        train IoU accuracy = 70.85 %
        motorcycle IoU accuracy = 54.29 %
        bicycle IoU accuracy = 75.42 %
IoU mean class accuracy -> TP / (TP+FN+FP) = 73.71 %
mean class recall -> TP / (TP+FN) = 80.63 %
mean class precision -> TP / (TP+FP) = 88.32 %
pixel accuracy = 94.88 %


SEG
>>> original
Errors:
        road IoU accuracy = 1.41 %
        sidewalk IoU accuracy = 3.63 %
        building IoU accuracy = 1.74 %
        wall IoU accuracy = 0.10 %
        fence IoU accuracy = 0.41 %
        pole IoU accuracy = 0.21 %
        traffic light IoU accuracy = 0.10 %
        traffic sign IoU accuracy = 0.78 %
        vegetation IoU accuracy = 0.61 %
        terrain IoU accuracy = 0.24 %
        sky IoU accuracy = 0.31 %
        person IoU accuracy = 0.28 %
        rider IoU accuracy = 0.14 %
        car IoU accuracy = 1.16 %
        truck IoU accuracy = 0.65 %
        bus IoU accuracy = 0.81 %
        train IoU accuracy = 0.07 %
        motorcycle IoU accuracy = 0.03 %
        bicycle IoU accuracy = 0.15 %
IoU mean class accuracy -> TP / (TP+FN+FP) = 0.68 %
mean class recall -> TP / (TP+FN) = 3.26 %
mean class precision -> TP / (TP+FP) = 2.83 %
pixel accuracy = 2.04 %
val: swift = 73.71  .  seg = 0.68


>>> decreased resolution
Errors:
        road IoU accuracy = 1.11 %
        sidewalk IoU accuracy = 2.91 %
        building IoU accuracy = 3.22 %
        wall IoU accuracy = 0.09 %
        fence IoU accuracy = 0.32 %
        pole IoU accuracy = 0.32 %
        traffic light IoU accuracy = 0.08 %
        traffic sign IoU accuracy = 0.96 %
        vegetation IoU accuracy = 0.56 %
        terrain IoU accuracy = 0.30 %
        sky IoU accuracy = 0.47 %
        person IoU accuracy = 0.30 %
        rider IoU accuracy = 0.06 %
        car IoU accuracy = 1.41 %
        truck IoU accuracy = 0.35 %
        bus IoU accuracy = 0.37 %
        train IoU accuracy = 0.06 %
        motorcycle IoU accuracy = 0.04 %
        bicycle IoU accuracy = 0.65 %
IoU mean class accuracy -> TP / (TP+FN+FP) = 0.72 %
mean class recall -> TP / (TP+FN) = 2.96 %
mean class precision -> TP / (TP+FP) = 2.85 %
pixel accuracy = 2.33 %
val: swift = 73.71  .  seg = 0.72


>> center crop
Errors:
        road IoU accuracy = 1.41 %
        sidewalk IoU accuracy = 5.01 %
        building IoU accuracy = 9.63 %
        wall IoU accuracy = 0.19 %
        fence IoU accuracy = 0.35 %
        pole IoU accuracy = 1.02 %
        traffic light IoU accuracy = 0.13 %
        traffic sign IoU accuracy = 1.03 %
        vegetation IoU accuracy = 0.78 %
        terrain IoU accuracy = 0.16 %
        sky IoU accuracy = 4.12 %
        person IoU accuracy = 0.59 %
        rider IoU accuracy = 0.19 %
        car IoU accuracy = 0.87 %
        truck IoU accuracy = 0.47 %
        bus IoU accuracy = 0.55 %
        train IoU accuracy = 0.11 %
        motorcycle IoU accuracy = 0.05 %
        bicycle IoU accuracy = 0.05 %
IoU mean class accuracy -> TP / (TP+FN+FP) = 1.41 %
mean class recall -> TP / (TP+FN) = 5.23 %
mean class precision -> TP / (TP+FP) = 4.72 %
pixel accuracy = 5.45 %
val: swift = 73.71  .  seg = 1.41

