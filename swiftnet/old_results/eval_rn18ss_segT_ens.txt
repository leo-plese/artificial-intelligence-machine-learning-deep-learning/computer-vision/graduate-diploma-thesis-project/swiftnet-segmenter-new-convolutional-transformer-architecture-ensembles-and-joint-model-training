>>> orig

Errors:
        road IoU accuracy = 95.99 %
        sidewalk IoU accuracy = 80.91 %
        building IoU accuracy = 91.85 %
        wall IoU accuracy = 50.63 %
        fence IoU accuracy = 53.09 %
        pole IoU accuracy = 60.34 %
        traffic light IoU accuracy = 68.12 %
        traffic sign IoU accuracy = 76.87 %
        vegetation IoU accuracy = 92.30 %
        terrain IoU accuracy = 60.93 %
        sky IoU accuracy = 94.74 %
        person IoU accuracy = 81.20 %
        rider IoU accuracy = 60.31 %
        car IoU accuracy = 94.52 %
        truck IoU accuracy = 71.53 %
        bus IoU accuracy = 85.03 %
        train IoU accuracy = 73.60 %
        motorcycle IoU accuracy = 57.49 %
        bicycle IoU accuracy = 76.03 %
IoU mean class accuracy -> TP / (TP+FN+FP) = 75.03 %
mean class recall -> TP / (TP+FN) = 81.65 %
mean class precision -> TP / (TP+FP) = 89.15 %
pixel accuracy = 95.32 %
val: swift-seg ensemble = 75.03




>>> decreased resolution
Errors:
        road IoU accuracy = 95.87 %
        sidewalk IoU accuracy = 80.46 %
        building IoU accuracy = 91.78 %
        wall IoU accuracy = 50.18 %
        fence IoU accuracy = 52.68 %
        pole IoU accuracy = 59.87 %
        traffic light IoU accuracy = 67.70 %
        traffic sign IoU accuracy = 76.48 %
        vegetation IoU accuracy = 92.22 %
        terrain IoU accuracy = 60.86 %
        sky IoU accuracy = 94.70 %
        person IoU accuracy = 80.93 %
        rider IoU accuracy = 60.14 %
        car IoU accuracy = 94.45 %
        truck IoU accuracy = 72.26 %
        bus IoU accuracy = 85.24 %
        train IoU accuracy = 72.41 %
        motorcycle IoU accuracy = 56.60 %
        bicycle IoU accuracy = 75.74 %
IoU mean class accuracy -> TP / (TP+FN+FP) = 74.77 %
mean class recall -> TP / (TP+FN) = 81.37 %
mean class precision -> TP / (TP+FP) = 89.08 %
pixel accuracy = 95.25 %
val: swift-seg ensemble = 74.77




>>> centercrop
Errors:
        road IoU accuracy = 93.88 %
        sidewalk IoU accuracy = 76.18 %
        building IoU accuracy = 91.14 %
        wall IoU accuracy = 45.28 %
        fence IoU accuracy = 49.50 %
        pole IoU accuracy = 58.91 %
        traffic light IoU accuracy = 67.67 %
        traffic sign IoU accuracy = 75.31 %
        vegetation IoU accuracy = 91.86 %
        terrain IoU accuracy = 56.62 %
        sky IoU accuracy = 94.43 %
        person IoU accuracy = 80.04 %
        rider IoU accuracy = 59.41 %
        car IoU accuracy = 93.99 %
        truck IoU accuracy = 70.93 %
        bus IoU accuracy = 83.94 %
        train IoU accuracy = 70.96 %
        motorcycle IoU accuracy = 53.88 %
        bicycle IoU accuracy = 74.89 %
IoU mean class accuracy -> TP / (TP+FN+FP) = 73.10 %
mean class recall -> TP / (TP+FN) = 79.53 %
mean class precision -> TP / (TP+FP) = 88.86 %
pixel accuracy = 94.51 %
val: swift-seg ensemble = 73.10

