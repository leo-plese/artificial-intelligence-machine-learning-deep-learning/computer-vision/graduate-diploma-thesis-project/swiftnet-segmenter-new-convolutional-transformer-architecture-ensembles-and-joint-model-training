#!/usr/bin/env bash
rm cylib.so

cython -a cylib.pyx -o cylib.cc

#g++ -shared -pthread -fPIC -fwrapv -O3 -Wall -fno-strict-aliasing \
#-I/home/lplese/miniconda3/envs/dipl/lib/python3.7/site-packages/numpy/core/include -I/home/lplese/miniconda3/envs/dipl/include/python3.7m -o cylib.so cylib.cc
g++ -shared -pthread -fPIC -fwrapv -O3 -Wall -fno-strict-aliasing \
-I/home/lplese/miniconda3/lib/python3.9/site-packages/numpy/core/include -I/home/lplese/miniconda3/include/python3.9 -o cylib.so cylib.cc
