import contextlib

import numpy as np
import torch
from tqdm import tqdm
from time import perf_counter
#from eval import process_batch

import lib.cylib as cylib

import segm.utils.torch as ptu
from segm.model.utils import inference
from torchvision.transforms import CenterCrop

__all__ = ['compute_errors', 'get_pred', 'evaluate_semseg', 'evaluate_semseg_swift_seg_center_crop', 'evaluate_semseg_swift_seg', 'evaluate_semseg_swift_seg_whole_imgs']


def compute_errors(conf_mat, class_info, verbose=True):
    num_correct = conf_mat.trace()
    num_classes = conf_mat.shape[0]
    total_size = conf_mat.sum()
    avg_pixel_acc = num_correct / total_size * 100.0
    TPFP = conf_mat.sum(1)
    TPFN = conf_mat.sum(0)
    FN = TPFN - conf_mat.diagonal()
    FP = TPFP - conf_mat.diagonal()
    class_iou = np.zeros(num_classes)
    class_recall = np.zeros(num_classes)
    class_precision = np.zeros(num_classes)
    per_class_iou = []
    if verbose:
        print('Errors:')
    for i in range(num_classes):
        TP = conf_mat[i, i]
        class_iou[i] = (TP / (TP + FP[i] + FN[i])) * 100.0
        if TPFN[i] > 0:
            class_recall[i] = (TP / TPFN[i]) * 100.0
        else:
            class_recall[i] = 0
        if TPFP[i] > 0:
            class_precision[i] = (TP / TPFP[i]) * 100.0
        else:
            class_precision[i] = 0

        class_name = class_info[i]
        per_class_iou += [(class_name, class_iou[i])]
        if verbose:
            print('\t%s IoU accuracy = %.2f %%' % (class_name, class_iou[i]))
    avg_class_iou = class_iou.mean()
    avg_class_recall = class_recall.mean()
    avg_class_precision = class_precision.mean()
    if verbose:
        print('IoU mean class accuracy -> TP / (TP+FN+FP) = %.2f %%' % avg_class_iou)
        print('mean class recall -> TP / (TP+FN) = %.2f %%' % avg_class_recall)
        print('mean class precision -> TP / (TP+FP) = %.2f %%' % avg_class_precision)
        print('pixel accuracy = %.2f %%' % avg_pixel_acc)
    return avg_pixel_acc, avg_class_iou, avg_class_recall, avg_class_precision, total_size, per_class_iou


def get_pred(logits, labels, conf_mat):
    _, pred = torch.max(logits.data, dim=1)
    pred = pred.byte().cpu()
    pred = pred.numpy().astype(np.int32)
    true = labels.numpy().astype(np.int32)
    cylib.collect_confusion_matrix(pred.reshape(-1), true.reshape(-1), conf_mat)


def mt(sync=False):
    if sync:
        torch.cuda.synchronize()
    return 1000 * perf_counter()

# for ENSEMBLE
def measure_macs_params(swift_model, seg_model):
    print("model.training", swift_model.training)
    from thop import profile
    # from torchvision.models import resnet101
    # model = resnet101().to(ptu.device)
    swift_model.eval()
    seg_model.eval()
    print("tr=", swift_model.training, seg_model.training)
    model_dev = next(swift_model.parameters()).device
    inp = torch.randn(1, 3, 1024, 2048).to(model_dev)
    logits_swift, _ = swift_model.forward(None, inp, (1024, 2048), (1024, 2048))
    swift_macs, swift_params = profile(swift_model, inputs=(None, inp, (1024, 2048), (1024, 2048)))
    seg_macs, seg_params = profile(seg_model, inputs=(inp, True, True, (0.5, 0.5, 0.5), (0.5, 0.5, 0.5), logits_swift))

    return swift_macs, swift_params, seg_macs, seg_params

# for ENSEMBLE
def measure_speed_eval_fps(
    swift_model,
    seg_model,
    bs,
    im_sh=(768,768),#(1024,2048),
    target_size_feats=(192,192),#(256,512),
    #im_sh=(1024,2048),
    #target_size_feats=(256,512),
    n=100):

    import torch
    import time

    swift_model.eval()
    seg_model.eval()

    warm_up = 10

    model_dev = next(swift_model.parameters()).device

    ###############
    torch.cuda.empty_cache()
    im = torch.randint(low=0, high=256, size=(bs, 3, *im_sh)).float().to(model_dev) # float32
    ###############
    print("IM",im.shape)
    with torch.no_grad():
        for i in range(warm_up):
            torch.cuda.empty_cache()
            #print(i)
            logits_seg = seg_model.forward(im, True, True, (0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
            logits_swift = swift_model.forward(None, im, target_size_feats, im_sh, False)
            logits = (logits_seg + logits_swift) / 2
            pred = torch.argmax(logits, dim=1)
            del logits_seg, logits_swift, logits, pred

    tot_time = 0
    with torch.no_grad():
        for _ in range(n):
            torch.cuda.empty_cache()

            torch.cuda.synchronize()
            start = time.time()

            logits_seg = seg_model.forward(im, True, True, (0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
            logits_swift = swift_model.forward(None, im, target_size_feats, im_sh, False)
            logits = (logits_seg + logits_swift) / 2
            pred = torch.argmax(logits, dim=1)

            torch.cuda.synchronize()
            end = time.time()

            tot_time += (end - start)

            del logits_seg, logits_swift, logits, pred

    return (bs * n) / tot_time

# >>> SEG
import cv2
import torch.nn.functional as F
import torchvision.transforms.functional as FT
def process_batch(
    model, batch, window_size, window_stride, window_batch_size, swift_num_pyramid_levels, mean, std, decrease_img_res=False, center_crop=False, is_ensemble=False
):
    #print("=======BTC==========")
    #for k in batch:
    #    print(k, "->", batch[k])
    ims = batch["image"].clone() #####
    #ims_orig = batch["image"]
    #ims = torch.zeros_like(ims_orig)
    #ims = ims_orig.clone()

    ####print("MEAN STD =", mean, std)

    ####print("ORIG")   
    ####print(type(ims),ims.shape,ims.dtype,"MIN=",torch.min(ims),"MAX=",torch.max(ims),torch.mean(ims),torch.std(ims))
    _, _, im_h, im_w = ims.shape  # 1024, 2048
    ####print("im_h im_w", im_h, im_w)
    ####print(ims)

    ims = ims / 255
    ####print("DIV BY 255")
    ####print(type(ims),ims.shape,ims.dtype,"MIN=",torch.min(ims),"MAX=",torch.max(ims),torch.mean(ims),torch.std(ims))
    ####print(ims)

    ims = FT.normalize(ims, mean, std)
    ####print("////////////////")
    ####print(ims.shape,torch.min(ims),torch.max(ims),torch.mean(ims),torch.std(ims))
    ####print(ims)


    # scaling (if specified)
    if decrease_img_res:
        ####print("\\\\\\\\ SCALED \\\\\\")
        im_scale_factor = 1 / 2 **(swift_num_pyramid_levels-1)
        ims = F.interpolate(ims, scale_factor=im_scale_factor, mode="bicubic")#, align_corners="None")
        ####print("1/4 of IMG")
        ####print(type(ims),ims.shape,ims.dtype,torch.min(ims),torch.max(ims),torch.mean(ims),torch.std(ims))
        ####print(ims)
    elif center_crop:
        ####print("\\\\\\\\ SCALED \\\\\\")
        im_scale_factor = 1 / 2 **(swift_num_pyramid_levels-1)
        crop_h, crop_w = int(im_h * im_scale_factor), int(im_w * im_scale_factor)
        ####print("crop_h crop_w", crop_h, crop_w)
        center_crop_transform = CenterCrop((crop_h, crop_w))
        ims = center_crop_transform(ims)
        ####print("1/4 CENTER CROP of IMG")
        ####print(type(ims),ims.shape,ims.dtype,torch.min(ims),torch.max(ims),torch.mean(ims),torch.std(ims))
        ####print(ims)
        

    # where to use??? y/n
    #_, _, rescaled_im_h, rescaled_im_w = ims.shape
    #print("rescaled_im_h rescaled_im_w", rescaled_im_h, rescaled_im_w)

    
    #ims_metas = batch["im_metas"]
    ori_shape = [torch.tensor([im_h]), torch.tensor([im_w]), torch.tensor([3])] #ims_metas[0]["ori_shape"]
    ori_shape = (ori_shape[0].item(), ori_shape[1].item())
    #filename = batch["im_metas"][0]["ori_filename"][0]

    model_without_ddp = model
    if ptu.distributed:
        model_without_ddp = model.module
    seg_pred = inference(
        model_without_ddp,
        ims.unsqueeze(0),####ims,
        #ims_metas,
        ori_shape,
        window_size,
        window_stride,
        window_batch_size,
    )
    seg_pred = seg_pred.unsqueeze(0)
    #print("SSSSSSSSSE",seg_pred.shape)
    return seg_pred

    #if is_ensemble:
    #    return seg_pred.unsqueeze(0)
    #else:
    #    print("000000000000",seg_pred.shape,torch.min(seg_pred),torch.max(seg_pred))
    #    seg_pred = seg_pred.data.argmax(0)
    #    print("111111111111",seg_pred.shape,torch.min(seg_pred),torch.max(seg_pred))
    
    #    return seg_pred#.unsqueeze(0).byte().cpu().numpy().astype(np.uint32)

# <<< SEG

def evaluate_semseg(model_swift, data_loader, class_info,
                    model_seg,
                    multiscale,
                    model_dir,
                    blend,
                    window_size,
                    window_stride,
                    window_batch_size,
                    save_images,
                    frac_dataset,
                    dataset_kwargs,
                    observers=(),
                    is_ensemble=False):
    img_norm_cfg = {"mean": (0.5, 0.5, 0.5), "std": (0.5, 0.5, 0.5)}

    model_swift.eval()
    pyr_levels = 3 #model_swift.backbone.pyramid_levels
    print("PYR LEVELS ====================",pyr_levels)

    im_h,im_w=1024,2048
    if multiscale:
        im_hw = (im_h,im_w)
        from torchvision.transforms import Resize
        scale_factors = [0.5,0.75,1.0,1.25,1.5,1.75,]
        resizes = [Resize((int(f*im_h),int(f*im_w))) for f in scale_factors]

    print("model swift device =", next(model_swift.parameters()).device)
    print("model seg device =", next(model_seg.parameters()).device)

    model_dev = next(model_swift.parameters()).device

    managers = [torch.no_grad()] + list(observers)
    with contextlib.ExitStack() as stack:
        for ctx_mgr in managers:
            stack.enter_context(ctx_mgr)
        conf_mat = np.zeros((model_swift.num_classes, model_swift.num_classes), dtype=np.uint64)

        if not is_ensemble:
            # >>> SEG
            conf_mat_seg = np.zeros((model_swift.num_classes, model_swift.num_classes), dtype=np.uint64)
            # <<< SEG

        for step, batch in tqdm(enumerate(data_loader), total=len(data_loader)):
            #print("=======BTC==========")

            #print("BEF IM",batch["image"].device)
            im = batch["image"].to(model_dev)
            #print("AFT IM",im.device)
            ims = []
            if multiscale:
                ##print("ORIG", im.shape)
                ims = [r(im) for r in resizes]
                ##print("RESIZED", [im.shape for im in ims])
                ims += [torch.flip(im, (-1,)) for im in ims]
                ##print("FLIPPED", [im.shape for im in ims])
            else:
                ims.append(im)

            seg_pred_sum = torch.zeros((1, model_seg.n_cls, im_h, im_w), device=im.device)
            #print("SEGPRED_SUM",seg_pred_sum.device,seg_pred_sum.shape)
            logits_sum = torch.zeros((1, model_swift.num_classes, im_h, im_w), device=im.device)
            #print("LOGS_SUM",logits_sum.device,logits_sum.shape)

            batch['original_labels'] = batch['original_labels'].numpy().astype(np.uint32)
            #print(torch.min(batch["image"]),torch.max(batch["image"]),torch.mean(batch["image"]),torch.std(batch["image"]))
            #print(batch["image"])

            batch_im = batch["image"]
            im_num = 0

            for im in ims:
                batch["image"] = im

                # >>> SEG
                ##print(">>> SEG")
                # Original image (i.e. resolution = highest)
                seg_pred = process_batch(model_seg, batch, window_size, window_stride, window_batch_size, pyr_levels, decrease_img_res=False, center_crop=False, is_ensemble=is_ensemble, **img_norm_cfg)
                # Decrease resolution
                #seg_pred = process_batch(model_seg, batch, window_size, window_stride, window_batch_size, pyr_levels, decrease_img_res=True, center_crop=False, is_ensemble=is_ensemble, **img_norm_cfg)
                # CenterCrop
                #seg_pred = process_batch(model_seg, batch, window_size, window_stride, window_batch_size, pyr_levels, decrease_img_res=False, center_crop=True, is_ensemble=is_ensemble, **img_norm_cfg)
                # <<< SEG

                #print("segpred",seg_pred.device,seg_pred.shape)
                if im_num >= 6:
                    seg_pred = torch.flip(seg_pred,(-1,))
                ##print("segpred segpredsum",seg_pred.shape,seg_pred_sum.shape)
                if multiscale:
                    seg_pred = F.interpolate(
                            seg_pred,
                            im_hw,
                            mode="bilinear",
                    )
                ##print("segpredAFT",seg_pred.shape)
                seg_pred_sum += seg_pred

                # >>> SWIFT
                ##print(">>> SWIFT")
                ####print(torch.min(batch["image"]),torch.max(batch["image"]),torch.mean(batch["image"]),torch.std(batch["image"]))
                ####print(batch["image"])
                logits, additional = model_swift.do_forward(batch, image_size=batch['original_labels'].shape[1:3])
                #print("logs",logits.shape)
                # <<< SWIFT

                ##print("log logsum",logits.shape,logits_sum.shape)
                if im_num >= 6:
                    logits = torch.flip(logits,(-1,))
                logits_sum += logits
                im_num += 1

            batch["image"] = batch_im
            seg_pred_sum /= len(ims)
            logits_sum /= len(ims)

            if not is_ensemble:
                # LEO - BEG: UNCOMMENT for segmaps!!!
                for o in observers:
                    o(seg_pred_sum, batch, additional=None, prefix="seg")
                # LEO - END: UNCOMMENT for segmaps!!!

                #print("CYLIB::::::::::",seg_pred_sum.shape,seg_pred_sum.data.argmax(1).shape)
                cylib.collect_confusion_matrix(seg_pred_sum.data.argmax(1).byte().cpu().numpy().astype(np.uint32).flatten(), batch['original_labels'].flatten(), conf_mat_seg)

            if is_ensemble:
                logits_seg_swift_avg = (logits_sum.data + seg_pred_sum.data) / 2
                pred = torch.argmax(logits_seg_swift_avg, dim=1).byte().cpu().numpy().astype(np.uint32)
            else:
                pred = torch.argmax(logits_sum.data, dim=1).byte().cpu().numpy().astype(np.uint32)

            # LEO - BEG: UNCOMMENT for segmaps!!!
            for o in observers:
                o(pred, batch, additional, prefix="swift")
            # LEO - END: UNCOMMENT for segmaps!!!

            cylib.collect_confusion_matrix(pred.flatten(), batch['original_labels'].flatten(), conf_mat)
            #

        print('')
        pixel_acc, iou_acc, recall, precision, _, per_class_iou = compute_errors(conf_mat, class_info, verbose=True)
        if not is_ensemble:
            pixel_acc_seg, iou_acc_seg, recall_seg, precision_seg, _, per_class_iou_seg = compute_errors(conf_mat_seg, class_info, verbose=True)

    model_swift.train()
    model_seg.train()

    if is_ensemble:
        return iou_acc, per_class_iou
    else:
        return iou_acc, per_class_iou, iou_acc_seg, per_class_iou_seg

#################################

from torchvision.transforms import CenterCrop
def evaluate_semseg_swift_seg_center_crop(model_swift, data_loader, class_info,
                                          model_seg,
                                          img_dev,
                                          multiscale=False,
                                          observers=()):

    model_seg.eval()

    model_swift.eval()
    pyr_levels = model_swift.backbone.pyramid_levels #3
    img_size_scale_fact = 2**(pyr_levels-1)
    print("PYR LEVELS ====================",pyr_levels)

    im_h,im_w=1024,2048
    crop_h, crop_w = im_h // img_size_scale_fact, im_w // img_size_scale_fact
    if multiscale:
        from torchvision.transforms import Resize
        scale_factors = [0.5,0.75,1.0,1.25,1.5,1.75,]
        resizes = [Resize((int(f*im_h),int(f*im_w))) for f in scale_factors]
        center_crop_transforms = [CenterCrop((int(f*crop_h),int(f*crop_w))) for f in scale_factors]
        center_crop_transforms += center_crop_transforms
    else:
        center_crop_transforms = [CenterCrop((crop_h,crop_w))]


    print("model swift device =", next(model_swift.parameters()).device)
    print("model seg device =", next(model_seg.parameters()).device)

    model_dev = next(model_swift.parameters()).device

    managers = [torch.no_grad()] + list(observers)
    with contextlib.ExitStack() as stack:
        for ctx_mgr in managers:
            stack.enter_context(ctx_mgr)
        conf_mat = np.zeros((model_swift.num_classes, model_swift.num_classes), dtype=np.uint64)

        for step, batch in tqdm(enumerate(data_loader), total=len(data_loader)):
            #print("=======BTC==========")
            #print(torch.min(batch["image"]),torch.max(batch["image"]),torch.mean(batch["image"]),torch.std(batch["image"]))
            #print(batch["image"])

            #print("im.shape",batch["image"].shape)
            ####im_h, im_w = batch["image"].shape[-2:]

            im = batch["image"].to(model_dev)

            ims = []
            if multiscale:
                ##print("ORIG", im.shape)
                ims = [r(im) for r in resizes]
                ##print("RESIZED", [im.shape for im in ims])
                ims += [torch.flip(im, (-1,)) for im in ims]
                ##print("FLIPPED", [im.shape for im in ims])
            else:
                ims.append(im)

            #print(len(ims)," IMS",[im.shape for im in ims])
            #print(len(center_crop_transforms), "CENTCRS",[c.size for c in center_crop_transforms])

            logits_sum = torch.zeros((1, model_swift.num_classes, im_h, im_w), device=im.device)

            batch['original_labels'] = batch['original_labels'].numpy().astype(np.uint32)

            batch_im = batch["image"]
            im_num = 0

            ####crop_h, crop_w = im_h//img_size_scale_fact, im_w//img_size_scale_fact
            #print("IM_H IM_W",im_h,im_w)
            #print("CROP_H CROP_W",crop_h,crop_w)
            center_crop_transform = CenterCrop((crop_h, crop_w))
            for im in ims:
                batch["image"] = im

                #print("im",im.shape)
                center_crop = center_crop_transforms[im_num](batch["image"])
                #print("center_crop",center_crop.shape)

                enc_out = model_seg.forward(center_crop.detach().requires_grad_(False).to(img_dev))
                #print("ENC_OUT ====",enc_out.shape)

                logits, additional = model_swift.do_forward(batch, enc_out, batch['original_labels'].shape[1:3])

                if im_num >= 6:
                    logits = torch.flip(logits,(-1,))
                #print("LOG logsum",logits.shape,logits_sum.shape)
                logits_sum += logits
                im_num += 1

            batch["image"] = batch_im
            logits_sum /= len(ims)

            pred = torch.argmax(logits_sum.data, dim=1).byte().cpu().numpy().astype(np.uint32)

            # LEO - BEG: UNCOMMENT for segmaps!!!
            for o in observers:
                o(pred, batch, additional)
            # LEO - END: UNCOMMENT for segmaps!!!

            cylib.collect_confusion_matrix(pred.flatten(), batch['original_labels'].flatten(), conf_mat)
            #

        print('')
        pixel_acc, iou_acc, recall, precision, _, per_class_iou = compute_errors(conf_mat, class_info, verbose=True)


    model_swift.train()
    model_seg.train()

    return iou_acc, per_class_iou


def evaluate_semseg_swift_seg(model_swift, data_loader, class_info,
                                            model_seg,
                                            img_dev,
                                            multiscale=False,
                                            observers=()):
    print("------------- MS",multiscale)
    model_seg.eval()

    model_swift.eval()
    pyr_levels = model_swift.backbone.pyramid_levels  # 3
    img_size_scale_fact = 1 / 2 ** (pyr_levels - 1)
    print("PYR LEVELS ====================", pyr_levels)

    im_h,im_w=1024,2048
    if multiscale:
        from torchvision.transforms import Resize
        scale_factors = [0.5,0.75,1.0,1.25,1.5,1.75,]
        resizes = [Resize((int(f*im_h),int(f*im_w))) for f in scale_factors]

    print("model swift device =", next(model_swift.parameters()).device)
    print("model seg device =", next(model_seg.parameters()).device)

    model_dev = next(model_swift.parameters()).device

    managers = [torch.no_grad()] + list(observers)
    with contextlib.ExitStack() as stack:
        for ctx_mgr in managers:
            stack.enter_context(ctx_mgr)
        conf_mat = np.zeros((model_swift.num_classes, model_swift.num_classes), dtype=np.uint64)

        for step, batch in tqdm(enumerate(data_loader), total=len(data_loader)):
            #print("=======BTC==========")
            #print(torch.min(batch["image"]),torch.max(batch["image"]),torch.mean(batch["image"]),torch.std(batch["image"]))
            # print(batch["image"])

            im = batch["image"].to(model_dev)

            ims = []
            if multiscale:
                ##print("ORIG", im.shape)
                ims = [r(im) for r in resizes]
                ##print("RESIZED", [im.shape for im in ims])
                ims += [torch.flip(im, (-1,)) for im in ims]
                ##print("FLIPPED", [im.shape for im in ims])
            else:
                ims.append(im)

            logits_sum = torch.zeros((1, model_swift.num_classes, im_h, im_w), device=im.device)

            batch['original_labels'] = batch['original_labels'].numpy().astype(np.uint32)

            batch_im = batch["image"]
            im_num = 0

            for im in ims:
                batch["image"] = im

                in_seg_img = batch["image"].detach().requires_grad_(False).to(img_dev)
                #print("in_seg_img0", in_seg_img.shape)
                in_seg_img = F.interpolate(in_seg_img, scale_factor=img_size_scale_fact, mode='bicubic', align_corners=None)
                #print("in_seg_img", in_seg_img.shape)

                enc_out = model_seg.forward(in_seg_img)
                #print("ENC_OUT ====", enc_out.shape)

                logits, additional = model_swift.do_forward(batch, enc_out, batch['original_labels'].shape[1:3])

                if im_num >= 6:
                    logits = torch.flip(logits,(-1,))

                #print("LOGS logsum",logits.shape,logits_sum.shape)
                logits_sum += logits
                im_num += 1

            batch["image"] = batch_im
            logits_sum /= len(ims)

            pred = torch.argmax(logits_sum.data, dim=1).byte().cpu().numpy().astype(np.uint32)

            # LEO - BEG: UNCOMMENT for segmaps!!!
            for o in observers:
                o(pred, batch, additional)
            # LEO - END: UNCOMMENT for segmaps!!!

            cylib.collect_confusion_matrix(pred.flatten(), batch['original_labels'].flatten(), conf_mat)
            #

        print('')
        pixel_acc, iou_acc, recall, precision, _, per_class_iou = compute_errors(conf_mat, class_info, verbose=True)

    model_swift.train()
    model_seg.train()

    return iou_acc, per_class_iou

def evaluate_semseg_swift_seg_whole_imgs(model_swift, data_loader, class_info,
                                            model_seg,
                                            img_dev,
                                            multiscale=False,
                                            observers=()):
    print("------------- MS",multiscale)
    model_seg.eval()

    model_swift.eval()

    im_h,im_w=1024,2048
    if multiscale:
        from torchvision.transforms import Resize
        scale_factors = [0.5,0.75,1.0,1.25,1.5,1.75,]
        resizes = [Resize((int(f*im_h),int(f*im_w))) for f in scale_factors]

    print("model swift device =", next(model_swift.parameters()).device)
    print("model seg device =", next(model_seg.parameters()).device)

    model_dev = next(model_swift.parameters()).device

    managers = [torch.no_grad()] + list(observers)
    with contextlib.ExitStack() as stack:
        for ctx_mgr in managers:
            stack.enter_context(ctx_mgr)
        conf_mat = np.zeros((model_swift.num_classes, model_swift.num_classes), dtype=np.uint64)

        for step, batch in tqdm(enumerate(data_loader), total=len(data_loader)):
            #print("=======BTC==========")
            #print(torch.min(batch["image"]),torch.max(batch["image"]),torch.mean(batch["image"]),torch.std(batch["image"]))
            # print(batch["image"])

            im = batch["image"].to(model_dev)

            ims = []
            if multiscale:
                ##print("ORIG", im.shape)
                ims = [r(im) for r in resizes]
                ##print("RESIZED", [im.shape for im in ims])
                ims += [torch.flip(im, (-1,)) for im in ims]
                ##print("FLIPPED", [im.shape for im in ims])
            else:
                ims.append(im)

            logits_sum = torch.zeros((1, model_swift.num_classes, im_h, im_w), device=im.device)

            batch['original_labels'] = batch['original_labels'].numpy().astype(np.uint32)

            batch_im = batch["image"]
            im_num = 0

            for im in ims:
                batch["image"] = im

                in_seg_img = batch["image"].detach().requires_grad_(False).to(img_dev)

                enc_out = model_seg.forward(in_seg_img)
                #print("ENC_OUT ====", enc_out.shape)

                logits, additional = model_swift.do_forward(batch, enc_out, batch['original_labels'].shape[1:3])

                if im_num >= 6:
                    logits = torch.flip(logits,(-1,))

                #print("LOGS logsum",logits.shape,logits_sum.shape)
                logits_sum += logits
                im_num += 1

            batch["image"] = batch_im
            logits_sum /= len(ims)

            pred = torch.argmax(logits_sum.data, dim=1).byte().cpu().numpy().astype(np.uint32)

            # LEO - BEG: UNCOMMENT for segmaps!!!
            for o in observers:
                o(pred, batch, additional)
            # LEO - END: UNCOMMENT for segmaps!!!

            cylib.collect_confusion_matrix(pred.flatten(), batch['original_labels'].flatten(), conf_mat)
            #

        print('')
        pixel_acc, iou_acc, recall, precision, _, per_class_iou = compute_errors(conf_mat, class_info, verbose=True)

    model_swift.train()
    model_seg.train()

    return iou_acc, per_class_iou

