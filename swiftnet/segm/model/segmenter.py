import torch
import torch.nn as nn
import torch.nn.functional as F

from segm.model.utils import padding, unpadding
from timm.models.layers import trunc_normal_

from einops import rearrange

import torchvision.transforms.functional as FT

class Segmenter(nn.Module):
    def __init__(
        self,
        encoder,
        decoder,
        n_cls,
        new_arch=True
    ):
        super().__init__()
        self.n_cls = n_cls
        self.patch_size = encoder.patch_size
        self.encoder = encoder
        self.decoder = decoder
        self.new_arch = new_arch

    @torch.jit.ignore
    def no_weight_decay(self):
        def append_prefix_no_weight_decay(prefix, module):
            return set(map(lambda x: prefix + x, module.no_weight_decay()))

        nwd_params = append_prefix_no_weight_decay("encoder.", self.encoder).union(
            append_prefix_no_weight_decay("decoder.", self.decoder)
        )
        return nwd_params

    def forward(self, im, return_masks=True, normalize_img=False, norm_mean=None, norm_std=None, swift_logits=None):
        if normalize_img:
            im = im / 255
            im = FT.normalize(im, norm_mean, norm_std)

        H_ori, W_ori = im.size(2), im.size(3)
        im = padding(im, self.patch_size)
        H, W = im.size(2), im.size(3)

        x = self.encoder(im, return_features=True)

        # remove CLS/DIST tokens for decoding
        num_extra_tokens = 1 + self.encoder.distilled
        x = x[:, num_extra_tokens:]

        enc_out = rearrange(x, "b (h w) n -> b n h w", h=(H // self.patch_size))
        ##print("enc_out ==", enc_out.shape)

        if not self.training and self.new_arch:
            return enc_out

        if not return_masks:
            ##print("DO NOT ret masks!!!")
            return enc_out, None

        masks = self.decoder(x, (H, W))

        masks = F.interpolate(masks, size=(H, W), mode="bilinear")
        masks = unpadding(masks, (H_ori, W_ori))

        ##print("masks =",masks.shape)
        # when training, return masks for decoder LOSS calc (see if needed as an auxiliary loss!)
        if self.new_arch:
            return enc_out, masks
        else:
            if swift_logits is None:
                return masks
            else:
                masks = (masks + swift_logits) / 2
                return masks

    def get_attention_map_enc(self, im, layer_id):
        return self.encoder.get_attention_map(im, layer_id)

    def get_attention_map_dec(self, im, layer_id):
        x = self.encoder(im, return_features=True)

        # remove CLS/DIST tokens for decoding
        num_extra_tokens = 1 + self.encoder.distilled
        x = x[:, num_extra_tokens:]

        return self.decoder.get_attention_map(x, layer_id)
