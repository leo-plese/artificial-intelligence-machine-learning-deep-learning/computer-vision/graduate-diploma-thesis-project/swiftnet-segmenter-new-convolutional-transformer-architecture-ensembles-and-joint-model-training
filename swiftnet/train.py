import argparse
import os
from pathlib import Path
import torch
import importlib.util
import datetime
import sys
from shutil import copy
import pickle
from time import perf_counter

# >>> SEG
from segm.utils import distributed
import segm.utils.torch as ptu
from segm import config
#from segm.data.utils import IGNORE_LABEL
from segm.model.factory import create_segmenter
from torch.nn.parallel import DistributedDataParallel as DDP
import yaml
from segm.optim.factory import create_optimizer, create_scheduler
from timm.utils import NativeScaler
from contextlib import suppress
from data.cityscapes import Cityscapes
from models.loss import BoundaryAwareFocalLoss
# <<< SEG

from evaluation import evaluate_semseg_swift_seg, evaluate_semseg_swift_seg_center_crop, evaluate_semseg_swift_seg_whole_imgs

def import_module(path):
    spec = importlib.util.spec_from_file_location("module", path)
    module = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(module)
    return module

def store(model, store_path, name):
    with open(store_path.format(name), 'wb') as f:
        torch.save(model.state_dict(), f)

def store_seg_model(trainer_obj):
    if ptu.dist_rank == 0:
        snapshot = dict(
            model=trainer_obj.seg_model.state_dict(),
            optimizer=trainer_obj.seg_optimizer.state_dict(),
            n_cls=trainer_obj.seg_model.n_cls,
            lr_scheduler=trainer_obj.seg_lr_scheduler.state_dict(),
        )
        snapshot["epoch"] = trainer_obj.epoch
        torch.save(snapshot, trainer_obj.checkpoint_dir / "seg_model.pt")


# LEO - BEG
def load(model, store_path, name):
    with open(store_path.format(name), 'rb') as f:
        model.load_state_dict(torch.load(f))
# LEO - END

def load_seg_model(trainer_obj):
    checkpoint_path = trainer_obj.checkpoint_dir / "seg_model.pt"
    checkpoint = torch.load(checkpoint_path, map_location="cpu")
    trainer_obj.seg_model.load_state_dict(checkpoint["model"])
    trainer_obj.seg_optimizer.load_state_dict(checkpoint["optimizer"])
    trainer.seg_lr_scheduler.load_state_dict(checkpoint["lr_scheduler"])

class Logger(object):
    def __init__(self, *files):
        self.files = files

    def write(self, obj):
        for f in self.files:
            f.write(obj)
            f.flush()  # If you want the output to be visible immediately

    def flush(self):
        for f in self.files:
            f.flush()


class Trainer:
    def __init__(self, conf, args, name, seg_model, seg_optimizer, seg_lr_scheduler, seg_amp_autocast, seg_log_dir, aux_seg_loss_weight=0.0, seg_center_crop=False, whole_im=False):
        self.aux_seg_loss_weight = aux_seg_loss_weight
        self.conf = conf
        using_hparams = hasattr(conf, 'hyperparams')
        print(f'Using hparams: {using_hparams}')
        self.hyperparams = self.conf
        self.args = args
        self.name = name
        self.model = self.conf.model
        self.optimizer = self.conf.optimizer

        self.dataset_train = self.conf.dataset_train
        self.dataset_val = self.conf.dataset_val
        self.loader_train = self.conf.loader_train
        self.loader_val = self.conf.loader_val

        # >>> SEG
        self.seg_criterion = BoundaryAwareFocalLoss(gamma=.5, num_classes=Cityscapes.num_classes, ignore_id=Cityscapes.num_classes) #torch.nn.CrossEntropyLoss(ignore_index=19) # HARDCODED for Cityscapes
        self.seg_model = seg_model
        self.seg_log_dir = seg_log_dir
        self.seg_optimizer = seg_optimizer
        self.seg_lr_scheduler = seg_lr_scheduler
        self.seg_amp_autocast = seg_amp_autocast
        self.epoch = 0
        self.seg_center_crop = seg_center_crop
        self.whole_im = whole_im
        # <<< SEG

    def __enter__(self):
        self.best_iou = -1
        self.best_iou_epoch = -1
        self.validation_ious = []
        self.experiment_start = datetime.datetime.now()

        if self.args.resume:
            self.experiment_dir = Path(self.args.resume)
            print(f'Resuming experiment from {args.resume}')
        else:
            self.experiment_dir = Path(self.args.store_dir) / (
                    self.experiment_start.strftime('%Y_%m_%d_%H_%M_%S_') + self.name)

        self.checkpoint_dir = self.experiment_dir / 'stored'
        self.store_path = str(self.checkpoint_dir / '{}.pt')

        if not self.args.dry and not self.args.resume:
            os.makedirs(str(self.experiment_dir), exist_ok=True)
            os.makedirs(str(self.checkpoint_dir), exist_ok=True)
            copy(self.args.config, str(self.experiment_dir / 'config.py'))

        copy(str(self.seg_log_dir / "variant.yml"), str(self.checkpoint_dir / "variant.yml"))

        if self.args.log and not self.args.dry:
            f = (self.experiment_dir / 'log.txt').open(mode='a')
            sys.stdout = Logger(sys.stdout, f)

        self.model.cuda(self.args.gpu_id)

        if self.args.resume:
            load(self.model, self.store_path, 'model')
            load(self.optimizer, self.store_path, 'optimizer')
            #load(self.seg_model, self.store_path, 'seg_model')

            load_seg_model(self)

            ###load(self.conf.lr_scheduler, self.store_path, 'lrscheduler')

        #self.model.cuda()

        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if not self.args.dry:
            store(self.model, self.store_path, 'model')
            store(self.optimizer, self.store_path, 'optimizer')
            #store(self.seg_model, self.store_path, 'seg_model')
            store_seg_model(self)


            ####store(self.conf.lr_scheduler, self.store_path, 'lrscheduler')

        if not self.args.dry:
            with open(f'{self.experiment_dir}/val_ious.pkl', 'wb') as f:
                pickle.dump(self.validation_ious, f)
            dir_iou = Path(self.args.store_dir) / (f'{self.best_iou:.2f}_'.replace('.', '-') + self.name)
            os.rename(self.experiment_dir, dir_iou)


    def train(self):
        print("ARGS:GPU =", self.args.gpu_id)
        print("model device =", next(self.model.parameters()).device)

        # >>> SEG
        img_dev = torch.device('cuda')
        to_calc_seg_loss = (self.aux_seg_loss_weight != 0)
        # <<< SEG

        num_epochs = self.hyperparams.epochs
        start_epoch = self.hyperparams.start_epoch if hasattr(self.hyperparams, 'start_epoch') else 0
        if start_epoch != 0:
            for i in range(start_epoch):
                self.conf.lr_scheduler.step()
        for epoch in range(start_epoch, num_epochs):
            self.epoch = epoch
            torch.cuda.empty_cache()  # LEO - ADD
            if hasattr(self.conf, 'epoch'):
                self.conf.epoch.value = epoch
                print(self.conf.epoch)
            self.model.train()
            # >>> SEG
            self.seg_model.train()
            # <<< SEG
            try:
                self.conf.lr_scheduler.step()
                print(f'Elapsed time: {datetime.datetime.now() - self.experiment_start}')
                for group in self.optimizer.param_groups:
                    print('LR: {:.4e}'.format(group['lr']))
                eval_epoch = ((epoch % self.conf.eval_each == 0) or (epoch == num_epochs - 1))  # and (epoch > 0)
                self.model.criterion.step_counter = 0
                print(f'Epoch: {epoch} / {num_epochs - 1}')
                if eval_epoch and not self.args.dry:
                    print("Experiment dir: %s" % self.experiment_dir)

                num_updates = epoch * len(self.loader_train)

                batch_iterator = iter(enumerate(self.loader_train))
                start_t = perf_counter()
                for step, batch in batch_iterator:
                    #print("===========BTC============")
                    #print(step,type(batch),"......",batch["image"].shape)

                    # torch.cuda.empty_cache() ######
                    self.optimizer.zero_grad()
                    self.seg_optimizer.zero_grad()

                    with self.seg_amp_autocast():
                        in_seg_img = batch["image"].detach().requires_grad_(False).to(img_dev)
                        #print("in_seg_img",in_seg_img.shape)

                        enc_out, masks = self.seg_model.forward(in_seg_img, return_masks=to_calc_seg_loss)

                    loss = self.model.loss(batch, enc_out)

                    if to_calc_seg_loss:
                        with self.seg_amp_autocast():
                            ##print("CALC SEG LOSS...")
                            in_seg_loss = batch["labels"].cuda()
                            #seg_loss = self.seg_criterion(masks, in_seg_loss)
                            seg_loss = self.seg_criterion(masks, in_seg_loss, batch)

                        total_loss = loss + seg_loss * self.aux_seg_loss_weight

                        if step % 20 == 0:
                            print("LOSS seg swift =", seg_loss.data.cpu().item(), loss.data.cpu().item(), "... TOTAL =", total_loss.data.cpu().item())
                    else:
                        ##print("ONLY SWIFT LOSS...")
                        total_loss = loss

                    total_loss.backward()

                    self.seg_optimizer.step()
                    self.optimizer.step()

                    num_updates += 1
                    self.seg_lr_scheduler.step_update(num_updates=num_updates)

                    if step % 80 == 0 and step > 0:
                        curr_t = perf_counter()
                        print(f'{(step * self.conf.batch_size) / (curr_t - start_t):.2f}fps')


                if not self.args.dry:
                    store(self.model, self.store_path, 'model')
                    store(self.optimizer, self.store_path, 'optimizer')
                    store_seg_model(self)
                if eval_epoch and self.args.eval:
                    print('Evaluating model')
                    # iou, per_class_iou = evaluate_semseg(self.model, self.loader_val, self.dataset_val.class_info)
                    if self.whole_im:
                        iou, per_class_iou = evaluate_semseg_swift_seg_whole_imgs(self.model, self.loader_val, self.dataset_val.class_info, self.seg_model, img_dev)
                    elif self.seg_center_crop:
                        iou, per_class_iou = evaluate_semseg_swift_seg_center_crop(self.model, self.loader_val, self.dataset_val.class_info, self.seg_model, img_dev)
                    else:
                        iou, per_class_iou = evaluate_semseg_swift_seg(self.model, self.loader_val, self.dataset_val.class_info, self.seg_model,img_dev)

                    self.validation_ious += [iou]
                    if self.args.eval_train:
                        print('Evaluating train')
                        # evaluate_semseg(self.model, self.loader_train, self.dataset_train.class_info)
                        if self.whole_im:
                            evaluate_semseg_swift_seg_whole_imgs(self.model, self.loader_train, self.dataset_train.class_info, self.seg_model, img_dev)
                        elif self.seg_center_crop:
                            evaluate_semseg_swift_seg_center_crop(self.model, self.loader_train, self.dataset_train.class_info, self.seg_model, img_dev)
                        else:
                            evaluate_semseg_swift_seg(self.model, self.loader_train, self.dataset_train.class_info, self.seg_model, img_dev)

                    if iou > self.best_iou:
                        self.best_iou = iou
                        self.best_iou_epoch = epoch
                        if not self.args.dry:
                            copy(self.store_path.format('model'), self.store_path.format('model_best'))
                            copy(self.store_path.format('seg_model'), self.store_path.format('seg_model_best'))
                    print(f'Best mIoU: {self.best_iou:.2f}% (epoch {self.best_iou_epoch})')

            except KeyboardInterrupt:
                break


parser = argparse.ArgumentParser(description='Detector train')
parser.add_argument('config', type=str, help='Path to configuration .py file')
parser.add_argument('--store_dir', default='saves/', type=str, help='Path to experiments directory')
parser.add_argument('--resume', default=None, type=str, help='Path to existing experiment dir')
parser.add_argument('--no-log', dest='log', action='store_false', help='Turn off logging')
parser.add_argument('--log', dest='log', action='store_true', help='Turn on train evaluation')
parser.add_argument('--no-eval-train', dest='eval_train', action='store_false', help='Turn off train evaluation')
parser.add_argument('--eval-train', dest='eval_train', action='store_true', help='Turn on train evaluation')
parser.add_argument('--no-eval', dest='eval', action='store_false', help='Turn off evaluation')
parser.add_argument('--eval', dest='eval', action='store_true', help='Turn on evaluation')
parser.add_argument('--dry-run', dest='dry', action='store_true', help='Don\'t store')
parser.add_argument('--gpu-id', dest='gpu_id', type=int, default=0)

parser.add_argument("--log-dir", type=str, help="logging directory") #
parser.add_argument("--dataset", type=str) #
parser.add_argument("--im-size", default=None, type=int, help="dataset resize size") #e
parser.add_argument("--crop-size", default=None, type=int) #
parser.add_argument("--backbone", default="", type=str) #
parser.add_argument("--decoder", default="", type=str) #
parser.add_argument("--normalization", default=None, type=str) #
parser.add_argument("--dropout", default=0.0, type=float) #
parser.add_argument("--drop-path", default=0.1, type=float) #

parser.add_argument("--optimizer", default="sgd", type=str)
parser.add_argument("-lr", "--learning-rate", default=None, type=float)
parser.add_argument("--weight-decay", default=0.0, type=float)
parser.add_argument("--scheduler", default="polynomial", type=str)
parser.add_argument("--amp", default=False)

parser.add_argument('--seg-center-crop', dest='seg_center_crop', type=int, default=0)
parser.add_argument('--whole-im', dest='whole_im', type=int, default=0)

parser.add_argument('--aux-seg-loss-weight', dest='aux_seg_loss_weight', type=float, default=0.0)

parser.add_argument("--net-port", dest="net_port", default=0, type=int)

parser.add_argument("--load-trained-seg-path", dest="load_trained_seg_path", type=str, default=None)

parser.set_defaults(log=True)
parser.set_defaults(eval_train=False)
parser.set_defaults(eval=True)

if __name__ == '__main__':
    args = parser.parse_args()
    conf_path = Path(args.config)
    conf = import_module(args.config)

    # >>> SEG
    # start distributed mode
    ptu.set_gpu_mode(True)
    distributed.init_process(net_port=args.net_port)

    # set up configuration
    cfg = config.load_config()
    model_cfg = cfg["model"][args.backbone]
    dataset_cfg = cfg["dataset"][args.dataset]
    if "mask_transformer" in args.decoder:
        decoder_cfg = cfg["decoder"]["mask_transformer"]
    else:
        decoder_cfg = cfg["decoder"][args.decoder]

    # model config
    if not args.im_size:
        im_size = dataset_cfg["im_size"]
    if not args.crop_size:
        crop_size = dataset_cfg.get("crop_size", im_size)

    model_cfg["image_size"] = (crop_size, crop_size)
    model_cfg["backbone"] = args.backbone
    model_cfg["dropout"] = args.dropout
    model_cfg["drop_path_rate"] = args.drop_path
    decoder_cfg["name"] = args.decoder
    model_cfg["decoder"] = decoder_cfg

    if args.normalization:
        model_cfg["normalization"] = args.normalization


    lr = dataset_cfg["learning_rate"]
    if args.learning_rate:
        lr = args.learning_rate


    # experiment config
    variant = dict(
        net_kwargs=model_cfg,
        log_dir=args.log_dir,
        optimizer_kwargs=dict(
            opt=args.optimizer,
            lr=lr,
            weight_decay=args.weight_decay,
            momentum=0.9,
            clip_grad=None,
            sched=args.scheduler,
            epochs=conf.epochs,
            min_lr=1e-5,
            poly_power=0.9,
            poly_step_size=1,
        ),
    )

    # model
    net_kwargs = variant["net_kwargs"]
    net_kwargs["n_cls"] = 19 # HARDCODED for Cityscapes!!! n_cls
    seg_model = create_segmenter(net_kwargs)
    seg_model.to(ptu.device)

    # optimizer
    optimizer_kwargs = variant["optimizer_kwargs"]
    optimizer_kwargs["iter_max"] = len(conf.dataset_train) * optimizer_kwargs["epochs"]
    optimizer_kwargs["iter_warmup"] = 0.0
    opt_args = argparse.Namespace()
    opt_vars = vars(opt_args)
    for k, v in optimizer_kwargs.items():
        opt_vars[k] = v
    seg_optimizer = create_optimizer(opt_args, seg_model)
    seg_lr_scheduler = create_scheduler(opt_args, seg_optimizer)
    num_iterations = 0
    seg_amp_autocast = suppress
    if args.amp:
        seg_amp_autocast = torch.cuda.amp.autocast

    if args.load_trained_seg_path:
        print("LOADING TRAINED SEG ******************")
        checkpoint_path = Path(args.load_trained_seg_path)
        checkpoint = torch.load(checkpoint_path, map_location="cpu")
        seg_model.load_state_dict(checkpoint["model"])
        seg_optimizer.load_state_dict(checkpoint["optimizer"])
        seg_lr_scheduler.load_state_dict(checkpoint["lr_scheduler"])


    if ptu.distributed:
        seg_model = DDP(seg_model, device_ids=[ptu.device], find_unused_parameters=True)

    # save config
    variant_str = yaml.dump(variant)
    print(f"Configuration:\n{variant_str}")
    log_dir = Path(args.log_dir)
    log_dir.mkdir(parents=True, exist_ok=True)
    with open(log_dir / "variant.yml", "w") as f:
        f.write(variant_str)
    # <<< SEG

    with Trainer(conf, args, conf_path.stem, seg_model, seg_optimizer, seg_lr_scheduler, seg_amp_autocast, log_dir, args.aux_seg_loss_weight, args.seg_center_crop, args.whole_im) as trainer:
        trainer.train()
