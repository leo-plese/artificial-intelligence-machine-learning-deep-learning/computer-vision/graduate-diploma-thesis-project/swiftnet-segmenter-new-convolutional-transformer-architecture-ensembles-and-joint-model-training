# >>> Swift
import argparse
from pathlib import Path
import importlib.util
from evaluation import evaluate_semseg
from evaluation import evaluate_semseg_swift_seg, evaluate_semseg_swift_seg_center_crop, evaluate_semseg_swift_seg_whole_imgs
from evaluation.evaluate import measure_macs_params, measure_speed_eval_fps
# <<< Swift

# >>> Seg
import sys
import click
import segm.utils.torch as ptu
from segm.model.factory import load_model
from segm.utils import distributed
from torch.nn.parallel import DistributedDataParallel as DDP
from segm import config as seg_config
import torch
# <<< Seg

# >>> Swift
def import_module(path):
    spec = importlib.util.spec_from_file_location("module", path)
    module = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(module)
    return module
# <<< Swift


### >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BEG - NEW ARCH !!!!!!!!!!
# >>> Seg
@click.command()
@click.argument("swift_config", type=str) # help='Path to SwiftNet configuration .py file')
@click.argument("model_path", type=str) # Seg config path
@click.argument("center_crop", default=0, type=int) # Seg config path
@click.argument("whole_im", default=0, type=int)
@click.option('--multiscale', type=int, default=0)
@click.option("--gpu-id", default=0, type=int)
def main_new_arch(
    swift_config,
    model_path,
    center_crop,
    whole_im,
    multiscale,
    gpu_id,
):
    # <<< Seg
    # >>> Swift
    print("::: BEG ::: swift")
    conf = import_module(swift_config)

    class_info = conf.dataset_val.class_info

    model_swift = conf.model.cuda(gpu_id)
    print("::: END ::: swift")
    # <<< Swift

    # >>> Seg
    print("::: BEG ::: seg")

    # start distributed mode
    ptu.set_gpu_mode(True)
    distributed.init_process()

    model_seg, variant = load_model(model_path)#,load_directly=True)

    ## beg
    #from segm.model.factory import create_segmenter
    #variant_path = Path(model_path).parent / "variant.yml"
    #with open(variant_path, "r") as f:
    #    variant = yaml.load(f, Loader=yaml.FullLoader)
    #net_kwargs = variant["net_kwargs"]
    # 
    #model_seg = create_segmenter(net_kwargs)
    ## end


    model_seg.to(ptu.device)
    if ptu.distributed:
        model_seg = DDP(model_seg, device_ids=[ptu.device], find_unused_parameters=True)

    model_seg.eval()
    print("::: END ::: seg")
    # <<< Seg

    ### >>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BEG - NEW ARCH !!!!!
    img_dev = torch.device('cuda')
    if whole_im:
        for loader, name in conf.eval_loaders:
            iou, per_class_iou = evaluate_semseg_swift_seg_whole_imgs(model_swift, loader, class_info, model_seg, img_dev, multiscale=(multiscale==1), observers=conf.eval_observers)
            print(f'{name}: {iou:.2f}')
    elif center_crop:
        for loader, name in conf.eval_loaders:
            iou, per_class_iou = evaluate_semseg_swift_seg_center_crop(model_swift, loader, class_info, model_seg, img_dev, multiscale=(multiscale==1), observers=conf.eval_observers)
            print(f'{name}: {iou:.2f}')
    else:
        for loader, name in conf.eval_loaders:
            iou, per_class_iou = evaluate_semseg_swift_seg(model_swift, loader, class_info, model_seg, img_dev, multiscale=(multiscale==1), observers=conf.eval_observers)
            print(f'{name}: {iou:.2f}')
    ### <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< END - NEW ARCH !!!!!

    # >>> Seg
    distributed.barrier()
    distributed.destroy_process()
    sys.exit(1)
    # <<< Seg
### <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< END - NEW ARCH !!!!!!!!!!


### >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BEG - ENSEMBLE EVAL
# >>> Swift
###parser = argparse.ArgumentParser(description='Detector train')
###parser.add_argument('--profile', dest='profile', action='store_true', help='Profile one forward pass')
###parser.add_argument('--gpu-id', dest='gpu_id', type=int, default=0)
# <<< Swift
# >>> Seg
@click.command()
@click.argument("swift_config", type=str) # help='Path to SwiftNet configuration .py file')
@click.argument("model_path", type=str) # Seg config path
@click.argument("dataset_name", type=str)
@click.option("--im-size", default=None, type=int)
@click.option("--multiscale/--singlescale", default=False, is_flag=True)
@click.option("--blend/--no-blend", default=True, is_flag=True)
@click.option("--window-size", default=None, type=int)
@click.option("--window-stride", default=None, type=int)
@click.option("--window-batch-size", default=4, type=int)
@click.option("--save-images/--no-save-images", default=False, is_flag=True)
@click.option("-frac-dataset", "--frac-dataset", default=1.0, type=float)
@click.option("--gpu-id", default=0, type=int)
@click.option("--measure-macs", type=int, default=0)
@click.option("--measure-speed-eval", type=int, default=0)
@click.option("--measure-speed-eval-batchsize", type=int, default=None)
def main(
    swift_config,
    model_path,
    dataset_name,
    im_size,
    multiscale,
    blend,
    window_size,
    window_stride,
    window_batch_size,
    save_images,
    frac_dataset,
    gpu_id,
    measure_macs,
    measure_speed_eval,
    measure_speed_eval_batchsize,
):
    # <<< Seg
    # >>> Swift
    print("::: BEG ::: swift")
    ###args = parser.parse_args()
    conf_path = Path(swift_config)
    conf = import_module(swift_config)

    class_info = conf.dataset_val.class_info

    model_swift = conf.model.cuda(gpu_id)
    print("::: END ::: swift")
    # <<< Swift

    # >>> Seg
    print("::: BEG ::: seg")
    model_dir = Path(model_path).parent

    # start distributed mode
    ptu.set_gpu_mode(True)
    distributed.init_process()

    model_seg, variant = load_model(model_path)
    model_seg.new_arch = False #True #False
    patch_size = model_seg.patch_size

    model_seg.to(ptu.device)
    if ptu.distributed:
        model_seg = DDP(model_seg, device_ids=[ptu.device], find_unused_parameters=True)

    cfg = seg_config.load_config()
    dataset_cfg = cfg["dataset"][dataset_name]
    normalization = variant["dataset_kwargs"]["normalization"]
    if im_size is None:
        im_size = dataset_cfg.get("im_size", variant["dataset_kwargs"]["image_size"])
    if window_size is None:
        window_size = variant["dataset_kwargs"]["crop_size"]
    if window_stride is None:
        window_stride = variant["dataset_kwargs"]["crop_size"] - 32

    dataset_kwargs = dict(
        dataset=dataset_name,
        image_size=im_size,
        crop_size=im_size,
        patch_size=patch_size,
        batch_size=1,
        num_workers=10,
        split="val",
        normalization=normalization,
        crop=False,
        rep_aug=False,
    )

    model_seg.eval()
    print("::: END ::: seg")
    # <<< Seg

    if measure_macs:
        #from evaluation import measure_macs_params
        swift_macs, swift_params, seg_macs, seg_params = measure_macs_params(conf.model, model_seg)

        print("MACS (SWIFT, SEG)", swift_macs, seg_macs, " --> Total:", swift_macs + seg_macs)
        print("PARAMS (SWIFT, SEG)", swift_params, seg_params)
        return
    elif measure_speed_eval:
        speed_result = measure_speed_eval_fps(conf.model, model_seg, measure_speed_eval_batchsize)

        print("FPS = ", speed_result)
        return
    ### >>>>>>>>>> BEG - ENSEMBLE EVAL
    is_ensemble = True #False #True #False #True # True for ensemble, False for separate

    print(">>>>>>>>>> EVAL")
    # >>> Swift
    for loader, name in conf.eval_loaders:
        # >>> Swift
        print("::: BEG ::: swift")
        if is_ensemble:
            iou, per_class_iou = evaluate_semseg(model_swift, loader, class_info,
            model_seg,
            multiscale,
            model_dir,
            blend,
            window_size,
            window_stride,
            window_batch_size,
            save_images,
            frac_dataset,
            dataset_kwargs,
            observers=conf.eval_observers,
            is_ensemble=is_ensemble)

            print(f'{name}: swift-seg ensemble = {iou:.2f}')
        else:
            iou, per_class_iou, iou_seg, per_class_iou_seg = evaluate_semseg(model_swift, loader, class_info,
            model_seg,
            multiscale,
            model_dir,
            blend,
            window_size,
            window_stride,
            window_batch_size,
            save_images,
            frac_dataset,
            dataset_kwargs,
            observers=conf.eval_observers,
            is_ensemble=is_ensemble)

            print(f'{name}: swift = {iou:.2f}  .  seg = {iou_seg:.2f}')

        ####print(f'{name}: seg = {iou_seg:.2f}')

        print("::: END ::: swift")
        # <<< Swift
    print("<<<<<<<<<<<< EVAL")
    # <<< Swift
    ### <<<<<<<<<<<< END - ENSEMBLE EVAL

    # >>> Seg
    distributed.barrier()
    distributed.destroy_process()
    sys.exit(1)
    # <<< Seg
### <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< END - ENSEMBLE EVAL

if __name__ == '__main__':
    eval_ens = False #True #False #True #False #True
    if eval_ens:
        main()
    else:
        main_new_arch()
